import 'package:esc_pos_utils/esc_pos_utils.dart';

extension ExtensionGenerator on Generator {
  List<int> printWithFontAndSize(
      List<int> input, PosFontType font, PosTextSize size) {
    return textEncoded(
      input,
      styles: PosStyles(
        fontType: font,
        height: size,
        width: size,
      ),
    );
  }

  List<int> printAllSizeWithFont(List<int> input, PosFontType font) {
    const ALL_SIZE = [
      PosTextSize.size1,
      PosTextSize.size2,
      PosTextSize.size3,
      PosTextSize.size4,
      PosTextSize.size5,
      PosTextSize.size6,
      PosTextSize.size7,
      PosTextSize.size8,
    ];
    return ALL_SIZE.fold(<int>[], (previousValue, element) {
      previousValue += printWithFontAndSize(input, font, element);
      return previousValue;
    });
  }

  List<int> printAllFontAndSize(List<int> input) {
    const ALL_FONT = [
      PosFontType.fontA,
      PosFontType.fontB,
    ];
    return ALL_FONT.fold(<int>[], (previousValue, element) {
      previousValue += printAllSizeWithFont(input, element);
      return previousValue;
    });
  }
}
