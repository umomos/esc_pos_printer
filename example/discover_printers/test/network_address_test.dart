import 'package:flutter_test/flutter_test.dart';
import 'package:ping_discover_network/ping_discover_network.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  final input = NetworkAddress('192.168.1.121', true);
  test('ip', () {
    expect(input.ip, '192.168.1.121');
  });

  test('exists', () {
    expect(input.exists, true);
  });
}
