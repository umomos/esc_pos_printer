import 'package:esc_pos_printer/esc_pos_printer.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group('a', () async {
    final profile = await CapabilityProfile.load();

    test('mm80', () async {
      const paper = PaperSize.mm80;
      // final profile = await CapabilityProfile.load();
      final input = NetworkPrinter(paper, profile);
    });

    test('mm58', () async {
      const paper = PaperSize.mm58;
      // final profile = await CapabilityProfile.load();
      final input = NetworkPrinter(paper, profile);
      // input.connect(host)
    });

    test('connect', () {
      //
    });

    test('disconnect', () {
      //
    });

    test('reset', () {
      //
    });

    test('text', () {
      //
    });

    test('setGlobalCodeTable', () {
      //
    });

    test('setGlobalFont', () {
      //
    });

    test('setStyles', () {
      //
    });

    test('rawBytes', () {
      //
    });

    test('emptyLines', () {
      //
    });

    test('feed', () {
      //
    });

    test('cut', () {
      //
    });

    test('printCodeTable', () {
      //
    });

    test('beep', () {
      //
    });

    test('reverseFeed', () {
      //
    });

    test('row', () {
      //
    });

    test('image', () {
      //
    });

    test('imageRaster', () {
      //
    });

    test('barcode', () {
      //
    });

    test('qrcode', () {
      //
    });

    test('drawer', () {
      //
    });

    test('hr', () {
      //
    });

    test('textEncoded', () {
      //
    });
  });
}
